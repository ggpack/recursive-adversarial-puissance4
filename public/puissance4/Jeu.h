#pragma once
#include "puissance4/Definitions.h"
#include <vector>

struct Regles
{
	virtual void deroulementDuJeu(Jeu& iJeu, Joueur& iJoueur1, Joueur& iJoueur2, Joueur*& oGagnant) const = 0;
};

struct Jeu
{
	virtual Regles& getRegles() = 0;
	virtual void toStream(std::ostream& oStream) const = 0;
	friend std::ostream& operator<<(std::ostream& oStream, const Jeu& iRhs);
	std::string toString() const;

	virtual void init(Joueur& iJoueur1, Joueur& iJoueur2) = 0;
};

