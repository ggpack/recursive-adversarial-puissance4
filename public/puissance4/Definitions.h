#pragma once
#include <memory>

#define Ptr(iClass)							\
class iClass;								\
typedef std::shared_ptr<iClass> iClass##Ptr;

class Strategy;
typedef std::shared_ptr<Strategy> StrategyPtr;

class Match;
typedef std::shared_ptr<Match> MatchPtr;

Ptr(Regles);
Ptr(ReglesP4);
Ptr(Puissance4);

class Jeu;
typedef std::shared_ptr<Jeu> JeuPtr;


class Puissance4;
typedef std::shared_ptr<Puissance4> P4Ptr;

class Joueur;
