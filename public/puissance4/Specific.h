#pragma once
#include "puissance4/Definitions.h"
#include "puissance4/Jeu.h"
#include "puissance4/Strategy.h"
#include <vector>


struct Couleur
{
	enum Type
	{
		Vide = 0,
		Jaune,
		Rouge,
		JauneHL,
		RougeHL
	};
	Couleur(Type iType = Vide): valeur(iType) {}
	bool operator==(const Couleur& iRhs) const { return valeur == iRhs.valeur; }
	bool operator==(const Type& iRhs) const { return valeur == iRhs; }
	bool operator!=(const Type& iRhs) const { return valeur != iRhs; }
	Couleur operator!() const { return Couleur(valeur == Jaune ? Rouge : Jaune); }

	friend std::ostream& operator<<(std::ostream& oStream, const Couleur& iRhs);

	static const std::string& display(const Couleur& iKey);
	//const std::string& display() const;
	std::string colorize(const std::string& iStr) const;
	Type valeur;
};



struct IssueP4
{
	enum Type
	{
		J2Gagne = 0,
		Egalite,
		PasFini,
		J1Gagne
	};
	friend std::ostream& operator<<(std::ostream& oStream, const IssueP4::Type& iRhs);
};

class Puissance4;
class P4Strategy;

struct ReglesP4 : Regles
{
	void deroulementDuJeu(Jeu& iJeu, Joueur& iJoueur1, Joueur& iJoueur2, Joueur*& oGagnant) const; // reimplem

	static bool detecteP4(Puissance4& iJeu, Couleur iCouleur);
	static void highlightP4(Puissance4& iJeu);
};

class Coor;

struct Puissance4 : Jeu
{
	typedef uint16_t Unit;
	typedef uint16_t Esprit;
	static const Esprit kIntelligenceMax = 5;

	Puissance4();

	bool checkCol(Unit idxColonne) const;
	bool addTo(Unit idxColonne, Couleur iCouleur);
	bool isFull() const;

	ReglesP4& getRegles();
	void toStream(std::ostream& oStream) const;

	void init(Joueur& iJoueur1, Joueur& iJoueur2);

	static const Unit kLargeur = 7; //7 Used by the strategies too.
	static const Unit kHauteur = 6; //6

	static const Unit kScoreVictoire = 4;

	static bool hasVal(const Coor& iCoor);
	Couleur& getVal(const Coor& iCoor);

	void replay() const;

	template<typename T>
	using Grille = std::array<std::array<T, kHauteur>, kLargeur>;

private:
	Grille<Couleur> _cnt;
	ReglesP4 _regles;
};
typedef Puissance4 P4;


struct P4Strategy : Strategy
{
	P4Strategy();
	Puissance4& getJeu(){ return getP4(); };
	Puissance4& getP4();
	Couleur couleurJoueur;
};

struct RandomStrategy : P4Strategy
{
	bool nextTurnImpl();
};/*
struct RobotStrategy : P4Strategy
{
	bool nextTurnImpl();
};*/

struct HumanStrategy : P4Strategy
{
	bool nextTurnImpl();
};

//P4::Unit calculeCoupOptimal(Puissance4 iJeu, Couleur iCouleur, P4::Esprit iEsprit);

struct IAStrategy : P4Strategy
{
	IAStrategy(P4::Esprit iE = P4::kIntelligenceMax);
	bool nextTurnImpl();
	P4::Esprit esprit;
};

struct IssuePossible
{
	typedef IssueP4::Type Type;

	IssuePossible(Type iType = IssueP4::Egalite);

	bool operator<(const IssuePossible& iRhs) const;
	bool operator==(const IssuePossible& iRhs) const;

	friend std::ostream& operator<<(std::ostream& oStream, const IssuePossible& iRhs);
	Type valeur;
};

struct Possibilite
{
	Possibilite(P4::Unit iChoix, IssuePossible iIssue, P4::Esprit iEsprit);

	friend std::ostream& operator<<(std::ostream& oStream, const Possibilite& iRhs);

	bool operator<(const Possibilite& iRhs) const;

	P4::Unit choix; // Colonne choisie.
	IssuePossible issue; // Score résumé, aggrégation des noeuds enfants, pondérés par la distance par rapport à l'instant présent.
	P4::Esprit esprit; // Le niveau de credibilite d'une solution
};

typedef std::vector<Possibilite> Possibs;


// Humain avec assistance predictive et réalité augmentée
/*struct CyborgStrategy : P4Strategy
{
	bool nextTurnImpl();
	static const uint8_t kPrediction = 5;
};
*/

