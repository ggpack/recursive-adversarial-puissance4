#pragma once
#include "puissance4/Definitions.h"

struct Joueur
{
	Joueur(StrategyPtr iStrat);
	void joue(/*params*/);

	MatchPtr defie(Joueur& iJoueur2, Jeu& iJeu);

//private:
	StrategyPtr _strat;
};
