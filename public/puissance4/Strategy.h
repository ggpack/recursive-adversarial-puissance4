#pragma once
#include "puissance4/Definitions.h"
#include "puissance4/Jeu.h"

struct Strategy
{
	Strategy();
	void nextTurn();
	virtual bool nextTurnImpl() = 0;

	virtual Jeu& getJeu() = 0;

	Jeu* jeu;
};

