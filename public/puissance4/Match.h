#pragma once
#include "puissance4/Definitions.h"


struct Match
{
	Match(Joueur& iJoueur1, Joueur& iJoueur2);

	void start();
	std::string issue() const;

	Jeu* jeu;
	Joueur* gagnant; // J1, J2, 0 (draw).

private:
	Joueur& _j1;
	Joueur& _j2;
};

