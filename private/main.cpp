#include <mdw_kit/Logging.h>
#include "puissance4/Strategy.h"
#include "puissance4/Joueur.h"
#include "puissance4/Match.h"
#include "puissance4/Specific.h"

using namespace std;

MainTesting
({
	Joueur aJ1(std::make_shared<HumanStrategy>());
	//Joueur aJ2(std::make_shared<HumanStrategy>());
	//Joueur aJ1(std::make_shared<IAStrategy>(P4::kIntelligenceMax - 1));
	//Joueur aJ2(std::make_shared<RobotStrategy>());
	Joueur aJ2(std::make_shared<IAStrategy>());

	Puissance4 aP4;

	MatchPtr aMatch = aJ1.defie(aJ2, aP4);
	aMatch->start();

	LOG_NOT("Issue du match: " << aMatch->issue());

	ReglesP4::highlightP4(aP4);

	LOG_INFO("Le Jeu final:\n" << aP4);
});
