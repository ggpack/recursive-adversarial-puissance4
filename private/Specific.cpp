#include "puissance4/Specific.h"
#include "SpecificDefinitions.h" // private lib classes.
#include "puissance4/Joueur.h"
#include <mdw_kit/Logging.h>
#include <algorithm>
#include <cstdlib>
#include <random>
#include <map>


#include <cstdlib>
#include <ctime>

using namespace std;

auto& GetRandomGenerator()
{
	static std::random_device aRandomDevice;
	static std::mt19937 aGenerator(aRandomDevice());
	return aGenerator;
}

P4::Unit GetRandomColumn() // Included.
{
	//std::uniform_int_distribution<> aDistri(0, Puissance4::kLargeur - 1);
	//return aDistri(GetRandomGenerator());

	std::srand(std::time(nullptr)); // use current time as seed for random generator
	return std::rand() % (Puissance4::kLargeur - 1);
}
bool GetRandomBool()
{
	std::uniform_int_distribution<> aDistri(0, 1);
	return aDistri(GetRandomGenerator());
}


string multStr(uint32_t iNombre, const string& iStr)
{
	stringstream aStream;
	for(uint32_t i = 0; i < iNombre; ++i){ aStream << iStr; }
	return aStream.str();
}

bool RandomStrategy::nextTurnImpl()
{
	mdw::kit::ScopedIndent aSI;

	P4::Unit aChosenColumn = 0;

	// Random parcour.
	std::vector<P4::Unit> shuffler(Puissance4::kLargeur);
	std::iota(shuffler.begin(), shuffler.end(), 0); // Remplissage
	std::random_shuffle(shuffler.begin(), shuffler.end());

	for(P4::Unit idx = 0; idx < shuffler.size(); ++idx)
	{
		aChosenColumn = shuffler[idx];

		if(getP4().checkCol(aChosenColumn)) // Que les coups valides.
		{
			break;
		}
	}

	return getP4().addTo(aChosenColumn, couleurJoueur);
}

bool HumanStrategy::nextTurnImpl()
{
	mdw::kit::ScopedIndent aSI;

	LOG_NOT("Voila la grille:\n" << *jeu);

	P4::Unit aChosenColumn = mdw::kit::AskInputUInt32("Quelle colonne ?");
	return getP4().addTo(aChosenColumn, couleurJoueur);
}

IAStrategy::IAStrategy(P4::Esprit iE) : esprit(iE){}


bool IAStrategy::nextTurnImpl()
{
	Puissance4& aJeu = getP4();
	if(!esprit)
	{
		RandomStrategy aRobot;
		aRobot.couleurJoueur = couleurJoueur;
		aRobot.jeu = &aJeu;
		return aRobot.nextTurnImpl();
	}
	else
	{
		P4::Unit aChosenColumn = calculeCoupOptimal(aJeu, couleurJoueur, esprit);
		return aJeu.addTo(aChosenColumn, couleurJoueur);
	}
}



IAStrategyChoisisseur::IAStrategyChoisisseur(P4::Unit iC, P4::Esprit iE):
	IAStrategy(iE),
	premierChoix(iC),
	premierTour(true)
{}
bool IAStrategyChoisisseur::nextTurnImpl()
{
	if(premierTour)
	{
		bool aReturn = this->getP4().addTo(premierChoix, this->couleurJoueur);
		premierTour = false;
		if(!aReturn){ LOG_ERROR("mauvais retour"); throw 4; }
		return aReturn;
	}
	else{ return IAStrategy::nextTurnImpl(); }
}



IssuePossible::IssuePossible(Type iType): valeur(iType) {}

bool IssuePossible::operator<(const IssuePossible& iRhs) const { return valeur < iRhs.valeur; }
bool IssuePossible::operator==(const IssuePossible& iRhs) const { return valeur == iRhs.valeur; }

std::ostream& operator<<(std::ostream& oStream, const IssuePossible& iRhs)
{
	return oStream << iRhs.valeur;
}


Possibilite::Possibilite(P4::Unit iChoix, IssuePossible iIssue, P4::Esprit iEsprit):
	choix(iChoix),
	issue(iIssue),
	esprit(iEsprit)
{}

std::ostream& operator<<(std::ostream& oStream, const Possibilite& iRhs)
{
	return oStream << "{choix: " << iRhs.choix << ", issue: " << iRhs.issue << ", esprit: " << iRhs.esprit << "}";
}

// Intelligence à mettre ici.
bool Possibilite::operator<(const Possibilite& iRhs) const
{
	if(issue == iRhs.issue)
	{
		bool plusSur = esprit > iRhs.esprit;
		bool mauvaiseNouvelle = issue < IssueP4::Egalite;
		return plusSur == mauvaiseNouvelle;
	}
	else
	{
		return issue < iRhs.issue;
	}
}

pair<IssueP4::Type, P4::Esprit> iterationsJeu(Puissance4& iJeu, P4Strategy& iJ1Strat, P4Strategy& iJ2Strat, P4::Esprit iEsprit)
{
	IssueP4::Type aIssue = IssueP4::PasFini;
	P4::Esprit aEsprit = iEsprit;

	while(aIssue == IssueP4::PasFini && aEsprit != 0)
	{
		if(iJeu.isFull()){ aIssue = IssueP4::Egalite; break; }

		iJ1Strat.nextTurn();
		if(ReglesP4::detecteP4(iJeu, iJ1Strat.couleurJoueur)){ aIssue = IssueP4::J1Gagne; break; }
		if(iJeu.isFull()){ aIssue = IssueP4::Egalite; break; }

		iJ2Strat.nextTurn();
		if(ReglesP4::detecteP4(iJeu, iJ2Strat.couleurJoueur)){ aIssue = IssueP4::J2Gagne; break; }

		--aEsprit;
	}
	if(aIssue == IssueP4::PasFini) aIssue = IssueP4::Egalite;
	return make_pair(aIssue, aEsprit);
}

P4::Unit meilleurChoix(Possibs& iPossibilites, bool iLog)
{
	std::random_shuffle(iPossibilites.begin(), iPossibilites.end()); // Moins previsible.
	std::sort(iPossibilites.begin(), iPossibilites.end());

	if(iLog)
	{
		for(Possibilite& aPoss : iPossibilites)
		{
			LOG_INFO("Possib: " << aPoss)
		}
	}

	if(iPossibilites.empty()){ LOG_ERROR("No solution"); throw 4;}
	return iPossibilites.back().choix;
}


P4::Unit calculeCoupOptimal(Puissance4& iJeu, Couleur iCouleur, P4::Esprit& iEsprit)
{
	Possibs aPossibilites;

	for(P4::Unit idx = 0; idx < P4::kLargeur; ++idx)
	{
		// Do not evaluate possibilities on full columns
		if(!iJeu.checkCol(idx)){ continue; }

		// Copie de Jeu evite les effets de bord
		Puissance4 aJeu = iJeu;

		IAStrategyChoisisseur aDeputy(idx, iEsprit - 1);
		aDeputy.couleurJoueur = iCouleur;
		aDeputy.jeu = &aJeu;


		IAStrategy aEnnemi(iEsprit - 1);
		aEnnemi.couleurJoueur = !iCouleur; // La couleur de l'ennemi, ca alterne.
		aEnnemi.jeu = &aJeu;

		pair<IssueP4::Type, P4::Esprit> aOut = iterationsJeu(aJeu, aDeputy, aEnnemi, aDeputy.esprit);
		aPossibilites.emplace_back(idx, aOut.first, aOut.second);
	}

	return meilleurChoix(aPossibilites, iEsprit == P4::kIntelligenceMax);
}


std::ostream& operator<<(std::ostream& oStream, const IssueP4::Type& iRhs)
{
	static const map<IssueP4::Type, string> kKeysDisplay =
	{
		{IssueP4::J2Gagne, "defaite"},
		{IssueP4::Egalite, "egalite"},
		{IssueP4::PasFini, "pas fini"},
		{IssueP4::J1Gagne, "victoire"}
	};
	auto aFoundIter = kKeysDisplay.find(iRhs);
	if(aFoundIter != kKeysDisplay.end())
	{
		oStream << aFoundIter->second;
	}
	else
	{
		oStream << "unknown " << uint32_t(iRhs);
	}
	return oStream;
}


void ReglesP4::deroulementDuJeu(Jeu& iJeu, Joueur& iJoueur1, Joueur& iJoueur2, Joueur*& oGagnant) const
{
	Puissance4& aP4 = dynamic_cast<Puissance4&>(iJeu);
	P4Strategy& aJ1Strat = dynamic_cast<P4Strategy&>(*iJoueur1._strat);
	P4Strategy& aJ2Strat = dynamic_cast<P4Strategy&>(*iJoueur2._strat);

	IssueP4::Type aIssue = iterationsJeu(aP4, aJ1Strat, aJ2Strat, 21).first;

	if(aIssue == IssueP4::J1Gagne)
	{
		oGagnant = &iJoueur1;
	}
	else if(aIssue == IssueP4::J2Gagne)
	{
		oGagnant = &iJoueur2;
	}
	LOG_INFO("Fin du jeu");
}


constexpr Coor::Coor(Base iX , Base iY): x(iX), y(iY) {}

Coor Coor::operator+(const Coor& iRhs) const { return Coor(x + iRhs.x, y + iRhs.y); }
bool Coor::operator==(const Coor& iRhs) const { return x == iRhs.x && y + iRhs.y; }
std::ostream& operator<<(std::ostream& oStream, const Coor& iRhs)
{
	return oStream << "[" << iRhs.x << "," << iRhs.y << "]";
}


DirectionalScore::DirectionalScore(const Scores& iSc): _scores(iSc)
{}


P4::Unit& DirectionalScore::operator/(Type idx){ return _scores[idx]; }

std::ostream& operator<<(std::ostream& oStream, const DirectionalScore& iRhs)
{
	return oStream << iRhs._scores[0] << iRhs._scores[1] << iRhs._scores[2] << iRhs._scores[3];
}

string DirectionalScore::toString(Couleur iColor)
{
	stringstream aStream;
	aStream << *this;
	return iColor.colorize(aStream.str());
}

const std::array<Coor, 4> DirectionalScore::offests({Coor(-1, 1), Coor(0, 1), Coor(1, 1), Coor(1, 0)});
const DirectionalScore DirectionalScore::zeros({0,0,0,0});


bool checkDir(const Coor& iCurrCase, DirectionalScore::Type iDir, Couleur iCouleur,
		Puissance4& iJeu, Puissance4::Grille<DirectionalScore>& oScores)
{
	Coor aNextCase = iCurrCase + DirectionalScore::offests[iDir];
	if(iJeu.hasVal(aNextCase) && iJeu.getVal(aNextCase) == iCouleur)
	{
		DirectionalScore& aCurrScore = oScores / iCurrCase;

		P4::Unit& aNewScore = oScores / aNextCase / iDir;
		aNewScore = aCurrScore / iDir + 1;

		if(aNewScore >= Puissance4::kScoreVictoire - 1)
		{
			LOG_DBG("GGGG next case " << aNextCase)
			LOG_DBG("GGGG next scores " << oScores / aNextCase)

			DBG_SCOPE
			(
				stringstream aStream; aStream << "\033[0m";
				Coor aCurrCase;
				for(aCurrCase.y = Puissance4::kHauteur - 1; aCurrCase.y >= 0; --aCurrCase.y)
				{
					aStream << multStr(Puissance4::kLargeur, "+----") << "+\n";
					for(aCurrCase.x = 0; aCurrCase.x < Puissance4::kLargeur; ++aCurrCase.x)
					{
						DirectionalScore& aCurrScore = oScores / aCurrCase;
						aStream << "|"  << aCurrScore.toString(iJeu.getVal(aCurrCase));
					}
					aStream << "|\n";
				}
				for(uint32_t i = 0; i < Puissance4::kLargeur; ++i){ aStream << "+--" << i << "-"; }
				aStream << "+\n";

				LOG_DBG("GGGG post vic\n" << aStream.str())
			)

			return true; // Stop au premier P4 trouvé.
		}
		else
		{
			// Recursif.
			return checkDir(aNextCase, iDir, iCouleur, iJeu, oScores);
		}

	}
	return false;
}

bool ReglesP4::detecteP4(Puissance4& iJeu, Couleur iCouleur)
{
	Puissance4::Grille<DirectionalScore> aScores = {};

	Coor aCurrCase; // Iterated.
	for(aCurrCase.y = 0; aCurrCase.y < Puissance4::kHauteur; ++aCurrCase.y)
	{
		for(aCurrCase.x = 0; aCurrCase.x < Puissance4::kLargeur; ++aCurrCase.x)
		{
			// Optim
			if(iJeu.getVal(aCurrCase) == iCouleur/* && ((aScores / aCurrCase)._scores == DirectionalScore::zeros._scores)*/)
			{
				bool aResult = checkDir(aCurrCase, DirectionalScore::HautGauche, iCouleur, iJeu, aScores)
							|| checkDir(aCurrCase, DirectionalScore::Haut,       iCouleur, iJeu, aScores)
							|| checkDir(aCurrCase, DirectionalScore::HautDroite, iCouleur, iJeu, aScores)
							|| checkDir(aCurrCase, DirectionalScore::Droite,     iCouleur, iJeu, aScores);
				if(aResult){ return true; }
			}
		}
	}
	DBG_SCOPE
	(
		stringstream aStream; aStream << "\033[0m";

		for(aCurrCase.y = Puissance4::kHauteur - 1; aCurrCase.y >= 0; --aCurrCase.y)
		{
			aStream << multStr(Puissance4::kLargeur, "+----") << "+\n";
			for(aCurrCase.x = 0; aCurrCase.x < Puissance4::kLargeur; ++aCurrCase.x)
			{
				DirectionalScore& aCurrScore = aScores / aCurrCase;
				aStream << "|"  << aCurrScore.toString(iJeu.getVal(aCurrCase));
			}
			aStream << "|\n";
		}
		for(uint32_t i = 0; i < Puissance4::kLargeur; ++i){ aStream << "+--" << i << "-"; }
		aStream << "+\n";

		LOG_DBG("Scores " << iCouleur << "\n" << aStream.str());
	)
	return false;
}

bool memeCouleur(const Couleur::Type& iLhs, const Couleur::Type& iRhs)
{
	return iLhs == iRhs
		|| (iLhs == Couleur::Jaune && iRhs == Couleur::JauneHL)
		|| (iLhs == Couleur::JauneHL && iRhs == Couleur::Jaune)
		|| (iLhs == Couleur::Rouge && iRhs == Couleur::RougeHL)
		|| (iLhs == Couleur::RougeHL && iRhs == Couleur::Rouge);
}

bool memeCouleur(const Couleur& iLhs, const Couleur& iRhs)
{
	return memeCouleur(iLhs.valeur, iRhs.valeur);
}


void highlight(Couleur& iCol)
{
	iCol.valeur = (memeCouleur(iCol.valeur, Couleur::Jaune) ? Couleur::JauneHL :
				  (memeCouleur(iCol.valeur, Couleur::Rouge) ? Couleur::RougeHL :
				  iCol.valeur));
}

void highlightDir(const Coor& iCurrCase, DirectionalScore::Type iDir, Couleur iCouleur, Puissance4& iJeu)
{
	highlight(iJeu.getVal(iCurrCase));
	Coor aNextCase = iCurrCase + DirectionalScore::offests[iDir];
	if(iJeu.hasVal(aNextCase) && memeCouleur(iJeu.getVal(aNextCase), iCouleur))
	{
		highlightDir(aNextCase, iDir, iCouleur, iJeu);
	}
}

void highlightCouleurP4(Puissance4& iJeu, Couleur iCouleur)
{
	Puissance4::Grille<DirectionalScore> aScores = {};

	Coor aCurrCase; // Iterated.
	for(aCurrCase.y = 0; aCurrCase.y < Puissance4::kHauteur; ++aCurrCase.y)
	{
		for(aCurrCase.x = 0; aCurrCase.x < Puissance4::kLargeur; ++aCurrCase.x)
		{
			if(iJeu.getVal(aCurrCase) == iCouleur /*&& ((aScores / aCurrCase)._scores == DirectionalScore::zeros._scores)*/)
			{
				// Pas de return pour tout highlighter.
				if(checkDir(aCurrCase, DirectionalScore::HautGauche, iCouleur, iJeu, aScores))
				{
					highlightDir(aCurrCase, DirectionalScore::HautGauche, iCouleur, iJeu);
				}
				if(checkDir(aCurrCase, DirectionalScore::Haut,       iCouleur, iJeu, aScores))
				{
					highlightDir(aCurrCase, DirectionalScore::Haut, iCouleur, iJeu);
				}
				if(checkDir(aCurrCase, DirectionalScore::HautDroite, iCouleur, iJeu, aScores))
				{
					highlightDir(aCurrCase, DirectionalScore::HautDroite, iCouleur, iJeu);
				}
				if(checkDir(aCurrCase, DirectionalScore::Droite,     iCouleur, iJeu, aScores))
				{
					highlightDir(aCurrCase, DirectionalScore::Droite, iCouleur, iJeu);
				}
			}
		}
	}
}
void ReglesP4::highlightP4(Puissance4& iJeu)
{
	highlightCouleurP4(iJeu, Couleur::Rouge);
	highlightCouleurP4(iJeu, Couleur::Jaune);
}


P4Strategy::P4Strategy():
	couleurJoueur(Couleur::Jaune)
{}

Puissance4& P4Strategy::getP4()
{
	if(!jeu){ LOG_ERROR("Pas de jeu!"); throw 4; }
	return dynamic_cast<Puissance4&>(*jeu);
}


Puissance4::Puissance4():
		_cnt({}) // To force zeros
{}


void Puissance4::init(Joueur& iJoueur1, Joueur& iJoueur2)
{
	try
	{
		P4Strategy& aJ1Strat = dynamic_cast<P4Strategy&>(*iJoueur1._strat);
		aJ1Strat.couleurJoueur = Couleur::Rouge;
		aJ1Strat.jeu = this;

		P4Strategy& aJ2Strat = dynamic_cast<P4Strategy&>(*iJoueur2._strat);
		aJ2Strat.couleurJoueur = Couleur::Jaune;
		aJ2Strat.jeu = this;
	}
	catch(...)
	{
		LOG_INFO("Les joueurs ne connaissent pas les regles !")
		throw;
	}

}

bool Puissance4::checkCol(P4::Unit idxColonne) const
{
	Coor aCase(idxColonne, Puissance4::kHauteur - 1);
	return idxColonne < _cnt.size() && _cnt / aCase == Couleur::Vide;
}

bool Puissance4::addTo(P4::Unit idxColonne, Couleur iCouleur)
{
	bool bCheck = checkCol(idxColonne);
	if(!bCheck)
	{
		LOG_INFO("Jouez une colonne libre dans [0, " << Puissance4::kLargeur << "], pas " << idxColonne << ". Rejouez !" )
	}
	else
	{
		for(auto& aVal : _cnt[idxColonne])
		{
			if(aVal == Couleur::Vide){ aVal = iCouleur; break; }
		}
	}
	return bCheck;
}

bool Puissance4::isFull() const
{
	return std::all_of(_cnt.begin(), _cnt.end(), [](auto& iCol){ return iCol[P4::kHauteur - 1] != Couleur::Vide; });
}


bool Puissance4::hasVal(const Coor& iCoor)
{
	return (iCoor.x >= 0 && std::abs(iCoor.x) < Puissance4::kLargeur)
		&& (iCoor.y >= 0 && std::abs(iCoor.y) < Puissance4::kHauteur);
}
Couleur& Puissance4::getVal(const Coor& iCoor)
{
	return _cnt / iCoor;
}


const string& Couleur::display(const Couleur& iKey)
{
	static const string kUnk = "UNK";
	static const map<Type, string> kKeysDisplay =
	{
		{Vide , "   "},
		{Jaune,     "\033[33m O \033[0m"},
		{Rouge,     "\033[31m O \033[0m"},
		{JauneHL, "\033[33;1m O \033[0m"},
		{RougeHL, "\033[31;1m O \033[0m"}
	};

	auto aFoundIter = kKeysDisplay.find(iKey.valeur);
	return aFoundIter != kKeysDisplay.end() ? aFoundIter->second : kUnk;
}


std::string Couleur::colorize(const std::string& iStr) const
{
	static const string kUnk = "";
	static const map<Type, string> kColors =
	{
		{Jaune, "\033[33m"},
		{Rouge, "\033[31m"}
	};
	auto aFoundIter = kColors.find(valeur);
	return (aFoundIter != kColors.end() ? aFoundIter->second : kUnk) + iStr + "\033[0m";
}

std::ostream& operator<<(std::ostream& oStream, const Couleur& iRhs)
{
	return oStream << iRhs.colorize("O");
}

void Puissance4::toStream(std::ostream& oStream) const
{
	oStream << "\033[0m";

	Coor aCurrCase;
	for(aCurrCase.y = Puissance4::kHauteur - 1; aCurrCase.y >= 0; --aCurrCase.y)
	{
		oStream << multStr(kLargeur, "+---") << "+\n";
		for(aCurrCase.x = 0; aCurrCase.x < Puissance4::kLargeur; ++aCurrCase.x)
		{
			oStream << "|" << Couleur::display(_cnt / aCurrCase);
		}
		oStream << "|\n";
	}

	for(uint32_t i = 0; i < kLargeur; ++i){ oStream << "+-" << i << "-"; }
	oStream << "+\n";
}

ReglesP4& Puissance4::getRegles()
{
	return _regles;
}


