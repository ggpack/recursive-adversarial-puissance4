#include "puissance4/Jeu.h"
#include "puissance4/Strategy.h"
#include "puissance4/Joueur.h"
#include <mdw_kit/Logging.h>

using namespace std;


string Jeu::toString() const
{
	stringstream aStream;
	toStream(aStream);
	return aStream.str();
}

std::ostream& operator<<(std::ostream& oStream, const Jeu& iRhs)
{
	iRhs.toStream(oStream);
	return oStream;
}
