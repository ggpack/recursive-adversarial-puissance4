#pragma once
#include "puissance4/Specific.h"

P4::Unit GetRandomColumn();
bool GetRandomBool();

std::string multStr(uint32_t iNombre, const std::string& iStr);

P4::Unit calculeCoupOptimal(Puissance4& iJeu, Couleur iCouleur, P4::Esprit& iEsprit);


struct IAStrategyChoisisseur : IAStrategy
{
	IAStrategyChoisisseur(P4::Unit iC, P4::Esprit iE);
	bool nextTurnImpl();
	P4::Unit premierChoix;
	bool premierTour;
};


std::pair<IssueP4::Type, P4::Esprit> iterationsJeu(Puissance4& iJeu, P4Strategy& iJ1Strat, P4Strategy& iJ2Strat, P4::Esprit iEsprit);

P4::Unit meilleurChoix(Possibs& iPossibilites, bool iLog = false);

// Copie de Jeu evite les effets de bord
P4::Unit calculeCoupOptimal(Puissance4& iJeu, Couleur iCouleur, P4::Esprit& iEsprit);


struct Coor
{
	typedef int16_t Base;

	constexpr Coor(Base iX = 0, Base iY = 0);

	Coor operator+(const Coor& iRhs) const;
	bool operator==(const Coor& iRhs) const;
	friend std::ostream& operator<<(std::ostream& oStream, const Coor& iRhs);

	Base x, y;
};

// Selection
template<typename T>
T& operator/(Puissance4::Grille<T>& iGrille, const Coor& iCoor)
{
	return iGrille[iCoor.x][iCoor.y];
}
template<typename T>
const T& operator/(const Puissance4::Grille<T>& iGrille, const Coor& iCoor)
{
	return iGrille[iCoor.x][iCoor.y];
}

struct DirectionalScore
{
	typedef std::array<P4::Unit, 4> Scores;

	DirectionalScore(const Scores& iSc = {});

	enum Type
	{
		HautGauche = 0,
		Haut       = 1,
		HautDroite = 2,
		Droite     = 3
	};

	P4::Unit& operator/(Type idx);
	friend std::ostream& operator<<(std::ostream& oStream, const DirectionalScore& iRhs);

	std::string toString(Couleur iColor);

	std::array<P4::Unit, 4> _scores;
	static const std::array<Coor, 4> offests; // Donne les diffs à ajouter pour se déplacer dans une direction.
	static const DirectionalScore zeros;      // Des 0 partout pour verifier si deja édité.
};


bool checkDir(const Coor& iCurrCase, DirectionalScore::Type iDir, Couleur iCouleur,
		Puissance4& iJeu, Puissance4::Grille<DirectionalScore>& oScores);

bool memeCouleur(const Couleur::Type& iLhs, const Couleur::Type& iRhs);
bool memeCouleur(const Couleur& iLhs, const Couleur& iRhs);

void highlight(Couleur& iCol);
void highlightDir(const Coor& iCurrCase, DirectionalScore::Type iDir, Couleur iCouleur, Puissance4& iJeu);
void highlightCouleurP4(Puissance4& iJeu, Couleur iCouleur);

