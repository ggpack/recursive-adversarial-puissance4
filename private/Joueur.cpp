#include "puissance4/Joueur.h"
#include "puissance4/Strategy.h"
#include "puissance4/Match.h"
#include "puissance4/Jeu.h"
#include <mdw_kit/Logging.h>

Joueur::Joueur(StrategyPtr iStrat):
	_strat(iStrat)
{}

void Joueur::joue(/*params*/)
{
	LOG_DBG("JOUE DONC");
	_strat->nextTurn();
}

MatchPtr Joueur::defie(Joueur& iJoueur2, Jeu& iJeu)
{
	MatchPtr aMatch = std::make_shared<Match>(*this, iJoueur2);

	aMatch->jeu = &iJeu;

	iJeu.init(*this, iJoueur2);

	return aMatch;
}
