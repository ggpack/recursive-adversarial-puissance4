#include "puissance4/Strategy.h"
#include "puissance4/Jeu.h"
#include <mdw_kit/Logging.h>


Strategy::Strategy():jeu(nullptr){}


void Strategy::nextTurn()
{
	bool aResult = false;
	while(!aResult)
	{
		aResult = nextTurnImpl();
	}
}

