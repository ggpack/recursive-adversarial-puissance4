#include "puissance4/Match.h"
#include "puissance4/Joueur.h"
#include "puissance4/Jeu.h"
#include <mdw_kit/Logging.h>

Match::Match(Joueur& iJoueur1, Joueur& iJoueur2):
	jeu(nullptr),
	gagnant(nullptr),
	_j1(iJoueur1),
	_j2(iJoueur2)
{}

void Match::start()
{
	Regles& aRegles = jeu->getRegles();
	aRegles.deroulementDuJeu(*jeu, _j1, _j2, gagnant);
}

std::string Match::issue() const
{
	std::string aReturn;
	if(gagnant)
	{
		aReturn = (gagnant == &_j1 ? "J1" : "J2");
		aReturn += " a gagné";
	}
	else
	{
		aReturn = "Egalité";
	}

	return aReturn;
}

