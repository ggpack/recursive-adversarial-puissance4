#include <ext_Catch/catch.hpp>
#include <SpecificDefinitions.h>
#include <puissance4/Specific.h>
#include <sstream>

TEST_CASE("FreeFunctions", "[Puissance4]")
{
	SECTION("multStr")
	{
		std::string aStr = "A,";
		REQUIRE(multStr(0, aStr) == "");
		REQUIRE(multStr(1, aStr) == aStr);
		REQUIRE(multStr(5, aStr) == "A,A,A,A,A,");
	}

	SECTION("GetRandomColumn")
	{
		P4::Unit aVal = GetRandomColumn();
		REQUIRE(aVal < 7); // Puissance4::kLargeur
	}
}

TEST_CASE("IAStrategyChoisisseur", "[Puissance4]")
{
	REQUIRE(0 == 0);
}

TEST_CASE("Coor", "[Puissance4]")
{
	Coor aCoorRef(1, -4);

	SECTION("operations")
	{
		REQUIRE(aCoorRef == Coor(1, -4));

		Coor aCoorTest1(-2, 3);
		Coor aCoorTest2 = aCoorRef + aCoorTest1;
		REQUIRE(aCoorTest2 == Coor(-1, -1));
	}

	SECTION("affichage")
	{
		std::stringstream aStream;
		aStream << aCoorRef;
		REQUIRE(aStream.str() == "[1,-4]");
	}
}

TEST_CASE("IssuePossible", "[Puissance4]")
{
	IssuePossible aIssueRef;

	SECTION("comparaison")
	{
		IssuePossible aIssueTest(IssueP4::J1Gagne);
		REQUIRE(aIssueRef < aIssueTest);

		aIssueTest.valeur = IssueP4::J2Gagne;
		REQUIRE(aIssueTest < aIssueRef);
	}

	SECTION("affichage")
	{
		std::stringstream aStream;
		aStream << aIssueRef;
		REQUIRE(aStream.str() == "egalite");
	}
}

TEST_CASE("Possibilite", "[Puissance4]")
{
	Possibilite aPossibRef(0, IssueP4::J1Gagne, 4);

	SECTION("comparaison")
	{
		Possibilite aPossibTest(0, IssueP4::J2Gagne, 1); // Issue differente
		REQUIRE(aPossibTest < aPossibRef);

		aPossibTest.issue = IssueP4::J1Gagne; // Esprit plus faible
		REQUIRE(aPossibTest < aPossibRef);

		aPossibTest.esprit = 5; // Esprit plus fort
		REQUIRE(aPossibRef < aPossibTest);

		aPossibTest.issue = IssueP4::J2Gagne; // Esprit plus fort mais issue plus faible.
		REQUIRE(aPossibTest < aPossibRef);

		aPossibRef.issue = IssueP4::J2Gagne; // Esprit plus fort et mauvaise issue.
		aPossibRef.esprit = 6;
		REQUIRE(aPossibRef < aPossibTest);
	}

	SECTION("affichage")
	{
		std::stringstream aStream;
		aStream << aPossibRef;
		REQUIRE(aStream.str() == "{choix: 0, issue: victoire, esprit: 4}");
	}
}

