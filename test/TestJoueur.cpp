#include <ext_Catch/catch.hpp>
#include <puissance4/Joueur.h>
#include "mocks/MockJeu.h"
#include "mocks/MockStrategy.h"

SCENARIO("Joueur basics", "[Joueur]")
{
	std::shared_ptr<MockStrategy> aStrat = std::make_shared<MockStrategy>();
	Joueur aJ1(aStrat);
	REQUIRE(aStrat->compte == 0);

	GIVEN("Creation d'un match par défi")
	{
		MockJeu aJeu;
		MatchPtr aMatch = aJ1.defie(aJ1, aJeu);

		REQUIRE(aMatch != nullptr);

		WHEN("Joueur joue une fois")
		{
			aJ1.joue();
			REQUIRE(aStrat->compte == 1);
		}
		WHEN("Joueur joue encore une fois")
		{
			aJ1.joue();
			aJ1.joue();
			REQUIRE(aStrat->compte == 2);
		}
	}
}

