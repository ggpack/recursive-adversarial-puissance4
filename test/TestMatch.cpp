#include <ext_Catch/catch.hpp>
#include <puissance4/Match.h>
#include <puissance4/Joueur.h>

SCENARIO("Match basics", "[Match]")
{
	StrategyPtr aStrat;
	Joueur aJ1(aStrat), aJ2(aStrat);

	GIVEN("Un match par defaut")
	{
		Match aMatch(aJ1, aJ2);
		REQUIRE(aMatch.issue() == "Egalité");

		WHEN("Joueur 1 gagne")
		{
			aMatch.gagnant = &aJ1;
			REQUIRE(aMatch.issue() == "J1 a gagné");
		}
		WHEN("Joueur 2 gagne")
		{
			aMatch.gagnant = &aJ2;
			REQUIRE(aMatch.issue() == "J2 a gagné");
		}
	}
}

