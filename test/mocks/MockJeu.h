#pragma once
#include <puissance4/Jeu.h>

struct MockRegles : Regles
{
	void deroulementDuJeu(Jeu& iJeu, Joueur& iJoueur1, Joueur& iJoueur2, Joueur*& oGagnant) const;
};

struct MockJeu : Jeu
{
	Regles& getRegles();
	void toStream(std::ostream& oStream) const;
	void init(Joueur& iJoueur1, Joueur& iJoueur2);

	MockRegles _regles;
};

