#include "mocks/MockJeu.h"
#include <ostream>

void MockRegles::deroulementDuJeu(Jeu&, Joueur& iJoueur1, Joueur&, Joueur*& oGagnant) const
{
	oGagnant = &iJoueur1;
}

Regles& MockJeu::getRegles(){ return _regles; }
void MockJeu::toStream(std::ostream& oStream) const { oStream << "MockJeu"; }
void MockJeu::init(Joueur&, Joueur&){}

