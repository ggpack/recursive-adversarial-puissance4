#pragma once
#include <puissance4/Strategy.h>

struct MockStrategy : Strategy
{
	MockStrategy();

	bool nextTurnImpl();
	Jeu& getJeu();

	uint32_t compte;
};

