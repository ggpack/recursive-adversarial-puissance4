#include "mocks/MockStrategy.h"


MockStrategy::MockStrategy():
		compte(0)
{}

bool MockStrategy::nextTurnImpl()
{
	++compte;
	return true;
}

Jeu& MockStrategy::getJeu()
{
	return *jeu;
}

