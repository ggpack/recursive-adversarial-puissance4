#include <ext_Catch/catch.hpp>
#include <puissance4/Specific.h>
#include <SpecificDefinitions.h>

SCENARIO("Couleur basics", "[Couleur]")
{
	GIVEN("Creation d'une couleur et comparaison")
	{
		Couleur aCouleur;
		REQUIRE(aCouleur.valeur == Couleur::Type::Vide);

		WHEN("on change la valeur")
		{
			aCouleur.valeur = Couleur::Type::Rouge;
			Couleur aCouleur2(Couleur::Type::JauneHL);
			Couleur aCouleur3(Couleur::Type::Rouge);

			THEN("la couleur se compare à sa nouvelle valeur")
			{
				REQUIRE(aCouleur == Couleur::Type::Rouge);
				REQUIRE(aCouleur != Couleur::Type::Jaune);
				REQUIRE(aCouleur == aCouleur3);
			}
		}
		WHEN("on inverse la valeur")
		{
			aCouleur = !aCouleur;
			THEN("la couleur est inversée")
			{
				REQUIRE(aCouleur == Couleur::Type::Jaune);
			}
		}
	}
	GIVEN("Affichage d'une couleur")
	{
		Couleur aCouleur4(Couleur::Type::Rouge);
		WHEN("on colorize du texte")
		{
			REQUIRE(aCouleur4.colorize("GGGG") == "\033[31mGGGG\033[0m");
		}
		WHEN("on affiche une case")
		{
			REQUIRE(Couleur::display(aCouleur4) == "\033[31m O \033[0m");
		}
	}
}

SCENARIO("Highlights", "[Couleur]")
{
	GIVEN("Comparaison de meme couleurs")
	{
		Couleur aCouleur1(Couleur::Type::RougeHL);
		Couleur aCouleur2(Couleur::Type::Rouge);
		REQUIRE(aCouleur1 != Couleur::Type::Rouge);
		REQUIRE(memeCouleur(aCouleur1, aCouleur2));
	}

	GIVEN("Highlight d'une couleur")
	{
		Couleur aCouleur(Couleur::Type::Rouge);
		highlight(aCouleur);
		REQUIRE(aCouleur != Couleur::Type::Rouge);
		REQUIRE( memeCouleur(aCouleur, Couleur(Couleur::Type::Rouge)) );
		REQUIRE( memeCouleur(aCouleur, Couleur(Couleur::Type::RougeHL)) );
	}
}

